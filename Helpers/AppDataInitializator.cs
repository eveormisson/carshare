﻿using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    public class AppDataInitializator
    {
        private static readonly string[] Roles = new[]{

            "User",
            "Admin"
        };

        public static void InitializeIdentity(UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            foreach (var role in Roles)
            {
                if (!roleManager.RoleExistsAsync(role).Result)
                {
                    roleManager.CreateAsync(new IdentityRole(role)).Wait();
                }
            }

            var userName = "admin@admin.ee";
            var userPass = "Kalamaja.01";

            if (userManager.FindByNameAsync(userName).Result == null)
            {
                var user = new ApplicationUser()
                {
                    UserName = userName,
                    Email = userName,
                    FirstName = "Admin",
                    LastName = "Admin",
                    Address = "Soo 33, Tallinn",
                    DrivingLicenceNr = "EV284860",
                    PhoneNumber = "55 66 778",
                };

                var res = userManager.CreateAsync(user, userPass).Result;
                if (res == IdentityResult.Success)
                {
                    foreach (var role in Roles)
                    {
                        userManager.AddToRoleAsync(user, role).Wait();
                    }
                }
            }


        }


        public static void InitializeAppDatabase(ApplicationDbContext context)
        {


            if (!context.ApplicationUsers.Any())
            {
                context.ApplicationUsers.Add(new ApplicationUser()
                {
                    FirstName = "Rene",
                    LastName = "Puu",
                    PhoneNumber = "55 666 777",
                    Address = "Kalju 5, Tallinn",
                    Email = "rene@rene",
                    Id = "kfkldkkvmvmfr586097076",
                    DrivingLicenceNr = "EE959670700808080",
                    UserName = "rene@rene"

                });

               

                context.SaveChanges();
            }

            //if (!context.EventStatuses.Any())
            //{
            //    context.EventStatuses.Add(new EventStatus()
            //    {
            //        EventStatusName = "confirmed"
            //    });

            //    context.EventStatuses.Add(new EventStatus()
            //    {
            //        EventStatusName = "pending"
            //    });

            //    context.EventStatuses.Add(new EventStatus()
            //    {
            //        EventStatusName = "cancelled"
            //    });

            //    context.SaveChanges();
            //}

            //if (!context.CarStatuses.Any())
            //{
            //    context.CarStatuses.Add(new CarStatus()
            //    {
            //        CarStatusName = "available"
            //    });

            //    context.CarStatuses.Add(new CarStatus()
            //    {
            //        CarStatusName = "out of the country"
            //    });

            //    context.CarStatuses.Add(new CarStatus()
            //    {
            //        CarStatusName = "in service"
            //    });

            //    context.CarStatuses.Add(new CarStatus()
            //    {
            //        CarStatusName = "in repair"
            //    });

            //    context.CarStatuses.Add(new CarStatus()
            //    {
            //        CarStatusName = "unavailable"
            //    });

            //    context.SaveChanges();
            //}

            //if (!context.ExpenseStatuses.Any())
            //{
            //    context.ExpenseStatuses.Add(new ExpenseStatus()
            //    {
            //        ExpenseStatusName = "paid"
            //    });

            //    context.ExpenseStatuses.Add(new ExpenseStatus()
            //    {
            //        ExpenseStatusName = "pending"
            //    });

            //    context.ExpenseStatuses.Add(new ExpenseStatus()
            //    {
            //        ExpenseStatusName = "waiting to be paid"
            //    });

            //    context.SaveChanges();



        }

            //if (!context.UserTypes.Any())
            //{
            //    context.UserTypes.Add(new UserType()
            //    {
            //        UserTypeName = "owner"
            //    });

            //    context.UserTypes.Add(new UserType()
            //    {
            //        UserTypeName = "user"
            //    });

            //    context.SaveChanges();
            //}


        }
        }
    
