﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class UserType
    {
        public int UserTypeId { get; set; }


        public int UserTypeNameId { get; set; }

        [ForeignKey(nameof(UserTypeNameId))]
        [Display(Name = nameof(Resources.Domain.UserType.UserTypeName), ResourceType = typeof(Resources.Domain.UserType))]
        public virtual MultiLangString UserTypeName { get; set; } = new MultiLangString();

        public virtual List<UserTypeInCar> UseerTypeInCars { get; set; } = new List<UserTypeInCar>();
    }
}
