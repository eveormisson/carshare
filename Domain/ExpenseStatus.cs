﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class ExpenseStatus
    {
        public int ExpenseStatusId { get; set; }

        public int ExpenseStatusNameId { get; set; }

        [ForeignKey(nameof(ExpenseStatusNameId))]
        [Display(Name = nameof(Resources.Domain.ExpenseStatus.ExpenseStatusName), ResourceType = typeof(Resources.Domain.ExpenseStatus))]
        public virtual MultiLangString ExpenseStatusName { get; set; } = new MultiLangString();

        public virtual List<Expense> Expenses { get; set; } = new List<Expense>();

    }
}
