﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Domain
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        
        [MaxLength(100)]
        [Display(Name = nameof(Resources.Domain.ApplicationUser.FirstName), ResourceType = typeof(Resources.Domain.ApplicationUser))]
        public string FirstName { get; set; }

        [MaxLength(100)]
        [Display(Name = nameof(Resources.Domain.ApplicationUser.LastName), ResourceType = typeof(Resources.Domain.ApplicationUser))]
        public string LastName { get; set; }

        [MaxLength(50)]
        [Display(Name = nameof(Resources.Domain.ApplicationUser.DrivingLicenceNr), ResourceType = typeof(Resources.Domain.ApplicationUser))]
        public string DrivingLicenceNr { get; set; }

        [MaxLength(100)]
        [Display(Name = nameof(Resources.Domain.ApplicationUser.Address), ResourceType = typeof(Resources.Domain.ApplicationUser))]
        public string Address { get; set; }

        [Display(Name = nameof(Resources.Domain.ApplicationUser.FirstLastName), ResourceType = typeof(Resources.Domain.ApplicationUser))]
        public string FirstLastName => FirstName + " " + LastName;

        [Display(Name = nameof(Resources.Domain.ApplicationUser.Email), ResourceType = typeof(Resources.Domain.ApplicationUser))]
        public override string Email { get => base.Email; set => base.Email = value; }

        [Display(Name = nameof(Resources.Domain.ApplicationUser.PhoneNumber), ResourceType = typeof(Resources.Domain.ApplicationUser))]
        public override string PhoneNumber { get => base.PhoneNumber; set => base.PhoneNumber = value; }

    }
}
