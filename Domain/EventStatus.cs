﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class EventStatus
    {
        public int EventStatusId { get; set; }

        public int EventStatusNameId { get; set; }

        [ForeignKey(nameof(EventStatusNameId))]
        [Display(Name = nameof(Resources.Domain.EventStatus.EventStatusName), ResourceType = typeof(Resources.Domain.EventStatus))]
        public virtual MultiLangString EventStatusName { get; set; } = new MultiLangString();

        public virtual List<UsingEvent> UsingEvents { get; set; } = new List<UsingEvent>();
    }
}
