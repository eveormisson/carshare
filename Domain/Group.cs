﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Group
    {
        public int GroupId { get; set; }

        [MaxLength(100)]
        [Display(Name = nameof(Resources.Domain.Group.GroupName), ResourceType = typeof(Resources.Domain.Group))]
        public string GroupName{ get; set; }

    }
}
