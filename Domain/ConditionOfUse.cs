﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class ConditionOfUse
    {
        public int ConditionOfUseId { get; set; }

        [MaxLength(128)]
        [Display(Name = nameof(Resources.Domain.ConditionOfUse.ConditionOfUseName), ResourceType = typeof(Resources.Domain.ConditionOfUse))]
        public string ConditionOfUseName { get; set; }

        [MaxLength(500)]
        [Display(Name = nameof(Resources.Domain.ConditionOfUse.ConditionOfUseDescription), ResourceType = typeof(Resources.Domain.ConditionOfUse))]
        public string ConditionOfUseDescription { get; set; }

        public int CarId { get; set; }

        [Display(Name = nameof(Resources.Domain.ConditionOfUse.Car), ResourceType = typeof(Resources.Domain.ConditionOfUse))]
        public Car Car { get; set; }

        [Required]
        [MaxLength(450)]
        public string ApplicationUserId { get; set; }

        [Display(Name = nameof(Resources.Domain.ConditionOfUse.ApplicationUser), ResourceType = typeof(Resources.Domain.ConditionOfUse))]
        public ApplicationUser ApplicationUser { get; set; }
    }
}
