﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class CarInGroup
    {
        public int CarInGroupId { get; set; }

        public int CarId { get; set; }

        [Display(Name = nameof(Resources.Domain.CarInGroup.Car), ResourceType = typeof(Resources.Domain.CarInGroup))]
        public Car Car { get; set; }

        public int GroupId { get; set; }

        [Display(Name = nameof(Resources.Domain.CarInGroup.Group), ResourceType = typeof(Resources.Domain.CarInGroup))]
        public Group Group { get; set; }
    }
}
