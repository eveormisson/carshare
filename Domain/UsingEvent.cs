﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class UsingEvent
    {
      
        public int UsingEventId { get; set; }

        [MaxLength(500)]
        [Display(Name = nameof(Resources.Domain.UsingEvent.EventDescription), ResourceType = typeof(Resources.Domain.UsingEvent))]
        public string EventDescription { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = nameof(Resources.Domain.UsingEvent.EventStartTime), ResourceType = typeof(Resources.Domain.UsingEvent))]
        public DateTime EventStartTime { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = nameof(Resources.Domain.UsingEvent.EventEndTime), ResourceType = typeof(Resources.Domain.UsingEvent))]
        public DateTime EventEndTime { get; set; }

        [MaxLength(150)]
        [Display(Name = nameof(Resources.Domain.UsingEvent.DrivingDestination), ResourceType = typeof(Resources.Domain.UsingEvent))]
        public string DrivingDestination { get; set; }

        public int CarId { get; set; }

        [Display(Name = nameof(Resources.Domain.UsingEvent.Car), ResourceType = typeof(Resources.Domain.UsingEvent))]
        public Car Car { get; set; }

        public int EventStatusId { get; set; }

        [Display(Name = nameof(Resources.Domain.UsingEvent.EventStatus), ResourceType = typeof(Resources.Domain.UsingEvent))]
        public EventStatus EventStatus { get; set; }

        [Required]
        [MaxLength(450)]
        public string ApplicationUserId { get; set; }

        [Display(Name = nameof(Resources.Domain.UsingEvent.EventCreator), ResourceType = typeof(Resources.Domain.UsingEvent))]
        public ApplicationUser EventCreator { get; set; }

    }
}
