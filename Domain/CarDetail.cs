﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class CarDetail
    {
        public int CarDetailId { get; set; }

        [MaxLength(128)]
        [Display(Name = nameof(Resources.Domain.CarDetail.CarDetailName), ResourceType = typeof(Resources.Domain.CarDetail))]
        public string CarDetailName { get; set; }

        [MaxLength(500)]
        [Display(Name = nameof(Resources.Domain.CarDetail.CarDetailDescription), ResourceType = typeof(Resources.Domain.CarDetail))]
        public string CarDetailDescription { get; set; }

        public int CarId { get; set; }

        [Display(Name = nameof(Resources.Domain.CarDetail.Car), ResourceType = typeof(Resources.Domain.CarDetail))]
        public Car Car { get; set; }

        [Required]
        [MaxLength(450)]
        public string ApplicationUserId { get; set; }

        [Display(Name = nameof(Resources.Domain.CarDetail.ApplicationUser), ResourceType = typeof(Resources.Domain.CarDetail))]
        public ApplicationUser ApplicationUser { get; set; }
    }
}
