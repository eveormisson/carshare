﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Expense
    {
        public int ExpenseId { get; set; }

        [MaxLength(128)]
        [Display(Name = nameof(Resources.Domain.Expense.ExpenseName), ResourceType = typeof(Resources.Domain.Expense))]
        public string ExpenseName { get; set; }

        [MaxLength(500)]
        [Display(Name = nameof(Resources.Domain.Expense.ExpenseDescription), ResourceType = typeof(Resources.Domain.Expense))]
        public string ExpenseDescription { get; set; }

        [Display(Name = nameof(Resources.Domain.Expense.ExpenseTotal), ResourceType = typeof(Resources.Domain.Expense))]
        public decimal ExpenseTotal { get; set; }

        public int CarId { get; set; }

        [Display(Name = nameof(Resources.Domain.Expense.Car), ResourceType = typeof(Resources.Domain.Expense))]
        public Car Car { get; set; }

        public int ExpenseStatusId { get; set; }

        [Display(Name = nameof(Resources.Domain.Expense.ExpenseStatus), ResourceType = typeof(Resources.Domain.Expense))]
        public ExpenseStatus ExpenseStatus { get; set; }

        [Required]
        [MaxLength(450)]
        public string ApplicationUserId { get; set; }

        [Display(Name = nameof(Resources.Domain.Expense.ApplicationUser), ResourceType = typeof(Resources.Domain.Expense))]
        public ApplicationUser ApplicationUser { get; set; }

    }
}
