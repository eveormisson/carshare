﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class Car
    {
        public int CarId { get; set; }

        [MaxLength(50)]
        [Display(Name = nameof(Resources.Domain.Car.CarBrand), ResourceType = typeof(Resources.Domain.Car))]
        public string CarBrand { get; set; }

        
        [MaxLength(50)]
        [MinLength(3, ErrorMessageResourceName = nameof(Resources.Shared.ErrorMessageMinLength), ErrorMessageResourceType = typeof(Resources.Shared))]
        [Display(Name = nameof(Resources.Domain.Car.CarLicensePlate), ResourceType = typeof(Resources.Domain.Car))]
        public string CarLicensePlate { get; set; }

        
        [MaxLength(50)]
        [Display(Name = nameof(Resources.Domain.Car.CarColour), ResourceType = typeof(Resources.Domain.Car))]
        public string CarColour { get; set; }

        
        [MaxLength(50)]
        [Display(Name = nameof(Resources.Domain.Car.CarYearOfIssue), ResourceType = typeof(Resources.Domain.Car))]
        public string CarYearOfIssue { get; set; }

        [MaxLength(200)]
        [Display(Name = nameof(Resources.Domain.Car.CarHomeAddress), ResourceType = typeof(Resources.Domain.Car))]
        public string CarHomeAddress { get; set; }

        //This ID is a string!
        [MaxLength(450)]
        [Required]
        public string OwnerId { get; set; }

        [ForeignKey(nameof(OwnerId))]
        [Display(Name = nameof(Resources.Domain.Car.Owner), ResourceType = typeof(Resources.Domain.Car))]
        public ApplicationUser Owner { get; set; }


        public int CarStatusId { get; set; }

        [Display(Name = nameof(Resources.Domain.Car.CarStatus), ResourceType = typeof(Resources.Domain.Car))]
        public CarStatus CarStatus { get; set; }

        public virtual List<CarDetail> CarDetails { get; set; } = new List<CarDetail>();

        public virtual List<ConditionOfUse> CondtitionsOfUse { get; set; } = new List<ConditionOfUse>();

        public virtual List<Expense> Expenses { get; set; } = new List<Expense>();

        public virtual List<UsingEvent> UsingEvents { get; set; } = new List<UsingEvent>();

        public virtual List<ApplicationUser> CarUsers { get; set; } = new List<ApplicationUser>();

        public string ColourBrandLicencePlate => CarColour + " " + CarBrand + " " + CarLicensePlate;
    }
}
