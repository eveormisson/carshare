﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class CarStatus
    {
        public int CarStatusId { get; set; }

        public int CarStatusNameId { get; set; }

        [ForeignKey(nameof(CarStatusNameId))]
        [Display(Name = nameof(Resources.Domain.CarStatus.CarStatusName), ResourceType = typeof(Resources.Domain.CarStatus))]
        public virtual MultiLangString CarStatusName { get; set; } = new MultiLangString();

        public virtual List<Car> Cars { get; set; } = new List<Car>();
    }
}
