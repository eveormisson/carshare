﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class UserTypeInCar
    {
        public int UserTypeInCarId { get; set; }

        public int UserTypeId { get; set; }

        [Display(Name = nameof(Resources.Domain.UserTypeInCar.UserType), ResourceType = typeof(Resources.Domain.UserTypeInCar))]
        public UserType UserType { get; set; }

        public string ApplicationUserId { get; set; }

        [Display(Name = nameof(Resources.Domain.UserTypeInCar.User), ResourceType = typeof(Resources.Domain.UserTypeInCar))]
        public ApplicationUser User { get; set; }

        public int CarId { get; set; }

        [Display(Name = nameof(Resources.Domain.UserTypeInCar.Car), ResourceType = typeof(Resources.Domain.UserTypeInCar))]
        public Car Car { get; set; }

        [Required]
        public string OwnerId { get; set; }

        [Display(Name = nameof(Resources.Domain.UserTypeInCar.CarOwner), ResourceType = typeof(Resources.Domain.UserTypeInCar))]
        [ForeignKey(nameof(OwnerId))]
        public ApplicationUser CarOwner { get; set; }
    }
}
