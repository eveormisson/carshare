﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class UserInGroup
    {
        public int UserInGroupId { get; set; }

        public int ApplicationUserId { get; set; }

        [Display(Name = nameof(Resources.Domain.UserInGroup.ApplicationUser), ResourceType = typeof(Resources.Domain.UserInGroup))]
        public ApplicationUser ApplicationUser { get; set; }

        public int GroupId { get; set; }

        [Display(Name = nameof(Resources.Domain.UserInGroup.Group), ResourceType = typeof(Resources.Domain.UserInGroup))]
        public Group Group { get; set; }
    }
}
