﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EventStatusRepository : EFRepository<EventStatus>, IEventStatusRepository
    {
        public EventStatusRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public async override Task<IEnumerable<EventStatus>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(EventStatus).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
                .Include(c => c.EventStatusName)
                .ThenInclude(c => c.Translations).ToListAsync();
        }

        public override IEnumerable<EventStatus> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(EventStatus).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.EventStatusName)
                .ThenInclude(c => c.Translations).ToList();
        }


        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.EventStatusId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.EventStatusId == keyValue);
        }

        public async Task<EventStatus> FindTranslationAsync(int id)
        {
            return await RepositoryDbSet
                .Include(c => c.EventStatusName)
                .ThenInclude(c => c.Translations)
                .SingleOrDefaultAsync(s => s.EventStatusId == id);
        }
    }
}
