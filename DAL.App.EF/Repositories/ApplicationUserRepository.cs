﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class ApplicationUserRepository : EFRepository<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<ApplicationUser> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(ApplicationUser).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet.ToList();
        }

        public async override Task<IEnumerable<ApplicationUser>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(ApplicationUser).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet.ToListAsync();
        }
        public bool ExistsByPrimaryKey(string keyValue)
        {
            return RepositoryDbSet.Any(e => e.Id == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(string keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.Id == keyValue);
        }

        public async Task<ApplicationUser> FindByGuidAsync(string id)
        {
            return await RepositoryDbSet.Where(u => u.Id == id).FirstOrDefaultAsync();
        }

        public List<ApplicationUser> GetAllApplicationUsers()
        {
            return RepositoryDbSet.ToList();
        }
    }
}
