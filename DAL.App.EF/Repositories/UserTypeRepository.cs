﻿using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.Interfaces.Repositories;
using System.Threading.Tasks;
using System.Linq;

namespace DAL.App.EF.Repositories
{
    public class UserTypeRepository : EFRepository<UserType>, IUserTypeRepository
    {
        public UserTypeRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(u => u.UserTypeId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(u => u.UserTypeId == keyValue);
        }

        public List<UserTypeInCar> FindUserType(string search)
        {
            throw new NotImplementedException();
            //return RepositoryDbSet.Where(c => c.UserTypeName.ToUpper()
            //.Contains(search.ToUpper())).ToList();
        }


        public async override Task<IEnumerable<UserType>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(UserType).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
                .Include(c => c.UserTypeName)
                .ThenInclude(c => c.Translations).ToListAsync();
        }

        public override IEnumerable<UserType> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(UserType).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.UserTypeName)
                .ThenInclude(c => c.Translations).ToList();
        }


        public async Task<UserType> FindTranslationAsync(int id)
        {
            return await RepositoryDbSet
                .Include(c => c.UserTypeName)
                .ThenInclude(c => c.Translations)
                .SingleOrDefaultAsync(m => m.UserTypeId == id);
        }
    }
}
