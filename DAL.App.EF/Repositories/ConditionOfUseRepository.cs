﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class ConditionOfUseRepository : EFRepository<ConditionOfUse>, IConditionOfUseRepository
    {
        public ConditionOfUseRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<ConditionOfUse> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(ConditionOfUse).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.Car)
                .Include(u => u.ApplicationUser)
                .ToList();
        }

        public async override Task<IEnumerable<ConditionOfUse>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(ConditionOfUse).FullName} {count}/{maxAllowed}");
                }

            }
            return await RepositoryDbSet
                .Include(c => c.Car)
                .Include(u => u.ApplicationUser)
                .ToListAsync();
        }

        public async Task<List<ConditionOfUse>> AllBelongingToUserAsync(string userId)
        {
            return await RepositoryDbSet
                .Where(o => o.ApplicationUserId == userId)
                .Include(s => s.Car).ToListAsync();
        }

        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.ConditionOfUseId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.ConditionOfUseId == keyValue);
        }

        public async Task<ConditionOfUse> FindWithUsersCarsAsync(int id)
        {
            return await RepositoryDbSet
                .Where(o => o.ConditionOfUseId == id)
                .Include(s => s.Car)
                .Include(c => c.ApplicationUser)
                .SingleOrDefaultAsync();
        }

        public async Task<ConditionOfUse> GetBelongingToUserAsync(int conditionOfUseId, string userId)
        {
            return await RepositoryDbSet
                .FirstOrDefaultAsync(a => a.ConditionOfUseId == conditionOfUseId && a.ApplicationUserId == userId);
        }

        public async Task<ConditionOfUse> GetBelongingToUserWithCarAsync(int conditionOfUseId, string userId)
        {
            return await RepositoryDbSet
               .Where(a => a.ConditionOfUseId == conditionOfUseId && a.ApplicationUserId == userId)
               .Include(a => a.Car)
               .FirstOrDefaultAsync();
        }
    }
}
