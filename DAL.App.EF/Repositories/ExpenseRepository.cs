﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class ExpenseRepository : EFRepository<Expense>, IExpenseRepository
    {
        public ExpenseRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<Expense> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(Expense).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(e => e.Car)
                .Include(e => e.ApplicationUser)
                .Include(e => e.ExpenseStatus)
                .ThenInclude(t => t.ExpenseStatusName)
                .ThenInclude(s => s.Translations)
                .ToList();
        }

        public async override Task<IEnumerable<Expense>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(Expense).FullName} {count}/{maxAllowed}");
                }
            }

            return await RepositoryDbSet
                .Include(e => e.Car)
                .Include(e => e.ApplicationUser)
                .Include(e => e.ExpenseStatus)
                .ThenInclude(t => t.ExpenseStatusName)
                .ThenInclude(s => s.Translations)
                .ToListAsync();
        }

        public async Task<List<Expense>> AllBelongingToUserAsync(string userId)
        {
            return await RepositoryDbSet
                .Where(e => e.ApplicationUserId == userId)
                .Include(e => e.Car)
                .Include(e => e.ApplicationUser)
                .Include(e => e.ExpenseStatus)
                .ThenInclude(t => t.ExpenseStatusName)
                .ThenInclude(s => s.Translations)
                .ToListAsync();
        }

        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.ExpenseId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.ExpenseId == keyValue);
        }

        public async Task<Expense> FindWithUsersCarsStatusesAsync(int id)
        {
            return await RepositoryDbSet
                .Where(o => o.ExpenseId == id)
                .Include(e => e.Car)
                .Include(e => e.ApplicationUser)
                .Include(e => e.ExpenseStatus)
                .ThenInclude(t => t.ExpenseStatusName)
                .ThenInclude(s => s.Translations)
                .SingleOrDefaultAsync();
        }

        public async Task<Expense> GetBelongingToUserAsync(int expenseId, string userId)
        {
            return await RepositoryDbSet
                .Where(a => a.ExpenseId == expenseId && a.ApplicationUserId == userId)
                .FirstOrDefaultAsync();
        }

        public async Task<Expense> GetBelongingToUserWithStatusCarAsync(int expenseId, string userId)
        {
            return await RepositoryDbSet
                .Where(a => a.ExpenseId == expenseId && a.ApplicationUserId == userId)
                .Include(e => e.Car)
                .Include(e => e.ApplicationUser)
                .Include(e => e.ExpenseStatus)
                .ThenInclude(t => t.ExpenseStatusName)
                .ThenInclude(s => s.Translations)
                .FirstOrDefaultAsync();
        }
    }
}
