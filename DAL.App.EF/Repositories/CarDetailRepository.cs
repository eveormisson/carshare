﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class CarDetailRepository : EFRepository<CarDetail>, ICarDetailRepository
    {
        public CarDetailRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<CarDetail> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(CarDetail).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.Car)
                .Include(o => o.ApplicationUser)
                .ToList();
        }

        public async override Task<IEnumerable<CarDetail>> AllAsync(int maxAllowed = 10)
        {

            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(CarDetail).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
                .Include(c => c.Car)
                .Include(o => o.ApplicationUser)
                .ToListAsync();
        }


        public async Task<List<CarDetail>> AllBelongingToUserAsync(string userId)
        {
            return await RepositoryDbSet
                .Where(d => d.ApplicationUserId == userId)
                .Include(s => s.Car).ToListAsync();
        }

        public async Task<List<CarDetail>> AllBelongingToCarAsync(int carId)
        {
            return await RepositoryDbSet
                .Where(d => d.CarId == carId)
                .Include(c => c.Car).ToListAsync();
        }

        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.CarDetailId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.CarDetailId == keyValue);
        }

        public async Task<CarDetail> GetBelongingToCarAsync(int carId)
        {
            return await RepositoryDbSet
                .FirstOrDefaultAsync(a => a.CarId == carId);
        }

     

        public async Task<CarDetail> GetBelongingToUserAsync(int carDetailId, string userId)
        {
            return await RepositoryDbSet
               .FirstOrDefaultAsync(a => a.CarDetailId == carDetailId && a.ApplicationUserId == userId);
        }

        public async Task<CarDetail> FindWithUsersCarsAsync(int id)
        {
            return await RepositoryDbSet
               .Where(o => o.CarDetailId == id)
               .Include(a => a.Car)
               .Include(c => c.ApplicationUser)
               .SingleOrDefaultAsync();
        }

        public async Task<CarDetail> GetBelongingToUserWithCarAsync(int carDetailId, string userId)
        {
            return await RepositoryDbSet
                .Where(a => a.CarDetailId == carDetailId && a.ApplicationUserId == userId)
                .Include(a => a.Car)
                .FirstOrDefaultAsync();
        }
    }
}
