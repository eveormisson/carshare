﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.App.EF.Repositories
{
    public class CarInGroupRepository : EFRepository<CarInGroup>, ICarInGroupRepository
    {
        public CarInGroupRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
