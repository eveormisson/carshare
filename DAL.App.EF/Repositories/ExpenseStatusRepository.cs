﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class ExpenseStatusRepository : EFRepository<ExpenseStatus>, IExpenseStatusRepository
    {
        public ExpenseStatusRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public async override Task<IEnumerable<ExpenseStatus>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(ExpenseStatus).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
                .Include(c => c.ExpenseStatusName)
                .ThenInclude(c => c.Translations).ToListAsync();
        }

        public override IEnumerable<ExpenseStatus> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(ExpenseStatus).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.ExpenseStatusName)
                .ThenInclude(c => c.Translations).ToList();
        }



        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.ExpenseStatusId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.ExpenseStatusId == keyValue);
        }

        public async Task<ExpenseStatus> FindTranslationAsync(int id)
        {
            return await RepositoryDbSet
               .Include(c => c.ExpenseStatusName)
               .ThenInclude(c => c.Translations)
               .SingleOrDefaultAsync(e => e.ExpenseStatusId == id);
        }
    }
}
