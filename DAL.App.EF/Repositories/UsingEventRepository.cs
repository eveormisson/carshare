﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class UsingEventRepository : EFRepository<UsingEvent>, IUsingEventRepository
    {
        public UsingEventRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<UsingEvent> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(UsingEvent).FullName} {count}/{maxAllowed}");
                }

            }
            return RepositoryDbSet
                .Include(u => u.Car)
                .Include(u => u.EventCreator)
                .Include(u => u.EventStatus)
                .ThenInclude(t => t.EventStatusName)
                .ThenInclude(s => s.Translations)
                .ToList();
        }

        public async override Task<IEnumerable<UsingEvent>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(UsingEvent).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
               .Include(u => u.Car)
               .Include(u => u.EventCreator)
               .Include(u => u.EventStatus)
               .ThenInclude(t => t.EventStatusName)
               .ThenInclude(s => s.Translations)
               .ToListAsync();
        }

        public async Task<List<UsingEvent>> AllBelongingToUserAsync(string userId)
        {
            return await RepositoryDbSet
                .Where(o => o.ApplicationUserId == userId)
                .Include(u => u.Car)
                .Include(u => u.EventCreator)
                .Include(u => u.EventStatus)
                .ThenInclude(t => t.EventStatusName)
                .ThenInclude(s => s.Translations)
                .ToListAsync();

        }

        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.UsingEventId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.UsingEventId == keyValue);
        }

        public List<UsingEvent> FindByDrivingDestination(string search)
        {
            return RepositoryDbSet.Where(u => u.DrivingDestination.ToUpper()
            .Contains(search.ToUpper())).ToList();
        }

        public async Task<UsingEvent> FindWithUsersCarsStatusesAsync(int id)
        {
            return await RepositoryDbSet
                .Where(o => o.UsingEventId == id)
                .Include(u => u.Car)
                .Include(u => u.EventCreator)
                .Include(u => u.EventStatus)
                .ThenInclude(t => t.EventStatusName)
                .ThenInclude(s => s.Translations)
                .SingleOrDefaultAsync();
        }

        public async Task<UsingEvent> GetBelongingToUserAsync(int usingEventId, string userId)
        {
            
            return await RepositoryDbSet
                .FirstOrDefaultAsync(a => a.UsingEventId == usingEventId && a.ApplicationUserId == userId);
        }

        public async Task<UsingEvent> GetBelongingToUserWithCarsUsersStatusesAsync(int usingEventId, string userId)
        {
            return await RepositoryDbSet
                .Where(a => a.UsingEventId == usingEventId && a.ApplicationUserId == userId)
                .Include(u => u.Car)
                .Include(u => u.EventCreator)
                .Include(u => u.EventStatus)
                .ThenInclude(t => t.EventStatusName)
                .ThenInclude(s => s.Translations)
                .FirstOrDefaultAsync();
        }
    }
}
