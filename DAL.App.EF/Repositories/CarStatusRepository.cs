﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class CarStatusRepository : EFRepository<CarStatus>, ICarStatusRepository
    {
        public CarStatusRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public async override Task<IEnumerable<CarStatus>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(CarStatus).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
                .Include(c => c.CarStatusName)
                .ThenInclude(c => c.Translations).ToListAsync();
        }

        public override IEnumerable<CarStatus> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(CarStatus).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.CarStatusName)
                .ThenInclude(c => c.Translations).ToList();
        }

        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.CarStatusId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.CarStatusId == keyValue);
        }

        public async Task<CarStatus> FindTranslationAsync(int id)
        {
            return await RepositoryDbSet
                .Include(c => c.CarStatusName)
                .ThenInclude(c => c.Translations)
                .SingleOrDefaultAsync(s => s.CarStatusId == id);
        }
    }
}