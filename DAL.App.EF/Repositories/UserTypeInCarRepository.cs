﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class UserTypeInCarRepository : EFRepository<UserTypeInCar>, IUserTypeInCarRepository
    {
        public UserTypeInCarRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public void AddUserToCar(string userId, int carId)
        {

            var t = new UserTypeInCar
            {
                UserTypeId = 2,
                ApplicationUserId = userId,
                CarId = carId
            };
        }

        public override IEnumerable<UserTypeInCar> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(UserTypeInCar).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.Car)
                .Include(c => c.User)
                .Include(c => c.UserType)
                .ThenInclude(t => t.UserTypeName)
                .ThenInclude(s => s.Translations)
                .ToList();
        }

        public async override Task<IEnumerable<UserTypeInCar>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(UserTypeInCar).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
                .Include(c => c.Car)
                .Include(c => c.User)
                .Include(c => c.CarOwner)
                .Include(c => c.UserType)
                .ThenInclude(t => t.UserTypeName)
                .ThenInclude(s => s.Translations)
                .ToListAsync();
        }

        public async Task<List<UserTypeInCar>> AllBelongingToUserAsync(string userId)
        {
            return await RepositoryDbSet
                .Where(o => o.ApplicationUserId == userId)
                .Include(u => u.User)
                .Include(s => s.Car)
                .Include(c => c.CarOwner)
                .Include(c => c.UserType)
                .ThenInclude(t => t.UserTypeName)
                .ThenInclude(s => s.Translations)
                .ToListAsync();
        }

        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(u => u.UserTypeInCarId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(u => u.UserTypeInCarId == keyValue);
               
        }

       
    }
}
