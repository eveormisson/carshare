﻿using DAL.App.Interfaces.Repositories;
using DAL.EF;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class CarRepository : EFRepository<Car>, ICarRepository
    {
        public CarRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<Car> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(Car).FullName} {count}/{maxAllowed}");
                }

            }

            return RepositoryDbSet
                .Include(c => c.Owner)
                .Include(c => c.CarStatus)
                .ThenInclude(t => t.CarStatusName)
                .ThenInclude(s => s.Translations)
                .ToList();
        }

        public async override Task<IEnumerable<Car>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(Car).FullName} {count}/{maxAllowed}");
                }

            }

            return await RepositoryDbSet
                .Include(c => c.Owner)
                .Include(c => c.CarStatus)
                .ThenInclude(t => t.CarStatusName)
                .ThenInclude(s => s.Translations)
                .ToListAsync();
        }

        public async Task<List<Car>> AllBelongingToUserAsync(string userId)
        {
            return await RepositoryDbSet
                .Where(o => o.OwnerId == userId)
                .Include(u => u.Owner)
                .Include(s => s.CarStatus)
                .ThenInclude(t => t.CarStatusName)
                .ThenInclude(s => s.Translations)
                .ToListAsync();
        }



        public bool ExistsByPrimaryKey(int keyValue)
        {
            return RepositoryDbSet.Any(e => e.CarId == keyValue);
        }

        public async Task<bool> ExistsByPrimaryKeyAsync(int keyValue)
        {
            return await RepositoryDbSet.AnyAsync(e => e.CarId == keyValue);
        }

        public List<Car> FindByBrand(string search)
        {
            return RepositoryDbSet.Where(c => c.CarBrand.ToUpper()
            .Contains(search.ToUpper())).ToList();
        }

        public List<Car> FindByOwner(string ownerId)
        {
            return RepositoryDbSet.Where(c => c.OwnerId == ownerId).ToList();
        }

        public List<Car> FindAllCars()
        {
            return RepositoryDbSet.ToList();
        }

        public async Task<Car> FindWithUsersStatusesAsync(int id)
        {
            return await RepositoryDbSet
                .Where(o => o.CarId == id)
                .Include(c => c.Owner)
                .Include(s => s.CarStatus)
                .ThenInclude(t => t.CarStatusName)
                .ThenInclude(s => s.Translations)
                .SingleOrDefaultAsync();
        }

        public async Task<Car> GetBelongingToUserAsync(int carId, string userId)
        {
            return await RepositoryDbSet
                .Where(a => a.CarId == carId && a.OwnerId == userId)
                .Include(a => a.CarStatus)
                .ThenInclude(t => t.CarStatusName)
                .ThenInclude(s => s.Translations)
                .FirstOrDefaultAsync();
        }

        public async Task<Car> GetBelongingToUserWithStatusUserAsync(int carId, string userId)
        {
            return await RepositoryDbSet
                .Where(a => a.CarId == carId && a.OwnerId == userId)
                .Include(a => a.Owner)
                .Include(a => a.CarStatus)
                .ThenInclude(t => t.CarStatusName)
                .ThenInclude(s => s.Translations)
                .FirstOrDefaultAsync();
        }
    }
}
