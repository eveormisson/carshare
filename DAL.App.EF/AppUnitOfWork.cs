﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using DAL.App.Interfaces.Repositories;
using DAL.EF;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.EF
{
    public class AppUnitOfWork : IAppUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public AppUnitOfWork(IDataContext dbContext, IRepositoryProvider repositoryProvider)
        {
            _dbContext = dbContext as ApplicationDbContext;
            if (_dbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }

            _repositoryProvider = repositoryProvider;
        }


        public IUserTypeRepository UserTypes =>
            GetCustomRepository<IUserTypeRepository>();

        public IUserTypeInCarRepository UserTypesInCars =>
            GetCustomRepository<IUserTypeInCarRepository>();

        public IApplicationUserRepository ApplicationUsers =>
             GetCustomRepository<IApplicationUserRepository>();

        public ICarRepository Cars =>
            GetCustomRepository<ICarRepository>();

        public ICarDetailRepository CarDetails =>
            GetCustomRepository<ICarDetailRepository>();

        public ICarStatusRepository CarStatus =>
            GetCustomRepository<ICarStatusRepository>();

        public IConditionOfUseRepository ConditionOfUses =>
            GetCustomRepository<IConditionOfUseRepository>();

        public IEventStatusRepository EventStatus =>
            GetCustomRepository<IEventStatusRepository>();

        public IExpenseRepository Expenses =>
            GetCustomRepository<IExpenseRepository>();

        public IExpenseStatusRepository ExpenseStatus =>
            GetCustomRepository<IExpenseStatusRepository>();

        public IUsingEventRepository UsingEvents =>
            GetCustomRepository<IUsingEventRepository>();

        public IMultiLangStringRepository MultiLangStrings =>
            GetCustomRepository<IMultiLangStringRepository>();

        public ITranslationRepository Translations =>
            GetCustomRepository<ITranslationRepository>();

        public IGroupRepository Groups =>
            GetCustomRepository<IGroupRepository>();

        public ICarInGroupRepository CarsInGroup =>
            GetCustomRepository<ICarInGroupRepository>();

        public IUserInGroupRepository UsersInGroup =>
            GetCustomRepository<IUserInGroupRepository>();

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class
        {
            return _repositoryProvider
                .ProvideEntityRepository<TEntity>();
        }

        public TRepositoryInterface GetCustomRepository<TRepositoryInterface>() where TRepositoryInterface : class
        {
            return _repositoryProvider
                .ProvideCustomRepository<TRepositoryInterface>();
        }
        
    }
}
