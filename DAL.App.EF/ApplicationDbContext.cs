﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDataContext
    {

        public DbSet<Domain.UserType> UserTypes { get; set; }
        public DbSet<Domain.ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Domain.UserTypeInCar> UserTypesInCars { get; set; }
        public DbSet<Domain.Car> Cars { get; set; }
        public DbSet<Domain.CarDetail> CarDetails { get; set; }
        public DbSet<Domain.CarStatus> CarStatuses { get; set; }
        public DbSet<Domain.ConditionOfUse> ConditionsOfUse { get; set; }
        public DbSet<Domain.EventStatus> EventStatuses { get; set; }
        public DbSet<Domain.Expense> Expenses { get; set; }
        public DbSet<Domain.ExpenseStatus> ExpenseStatuses { get; set; }
        public DbSet<Domain.UsingEvent> UsingEvents { get; set; }
        public DbSet<Domain.MultiLangString> MultiLangStrings { get; set; }
        public DbSet<Domain.UserInGroup> UsersInGroups { get; set; }
        public DbSet<Domain.Group> Groups { get; set; }
        public DbSet<Domain.CarInGroup> CarsInGroups { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {

            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
