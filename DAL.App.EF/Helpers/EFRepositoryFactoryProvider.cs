﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using DAL.App.Interfaces.Repositories;
using DAL.EF;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryFactoryProvider : IRepositoryFactoryProvider
    {
        private static readonly Dictionary<Type, Func<IDataContext, object>> _customRepositoryFactories = GetCustomRepoFactories();

        public Func<IDataContext, object> GetFactoryForStandarRepo<TEntity>() where TEntity : class
        {
            return (context) => new EFRepository<TEntity>(context as ApplicationDbContext);
        }

        public Func<IDataContext, object> GetFactoryForCustomRepo<TRepositoryInterface>() where TRepositoryInterface : class
        {

            _customRepositoryFactories.TryGetValue(typeof(TRepositoryInterface), out var factory);
            return factory;
        }

        private static Dictionary<Type, Func<IDataContext, object>> GetCustomRepoFactories()
        {
            return new Dictionary<Type, Func<IDataContext, object>>()
            {

                { typeof(IUserTypeRepository), (dataContext) => new UserTypeRepository(dataContext as ApplicationDbContext) },
                { typeof(IUserTypeInCarRepository), (dataContext) => new UserTypeInCarRepository(dataContext as ApplicationDbContext)},
                { typeof(IApplicationUserRepository), (dataContext) => new ApplicationUserRepository(dataContext as ApplicationDbContext)},
                { typeof(ICarRepository), (dataContext) =>  new CarRepository(dataContext as ApplicationDbContext)},
                { typeof(ICarDetailRepository), (dataContext) =>  new CarDetailRepository(dataContext as ApplicationDbContext)},
                { typeof(ICarStatusRepository), (dataContext) =>  new CarStatusRepository(dataContext as ApplicationDbContext)},
                { typeof(IConditionOfUseRepository), (dataContext) =>  new ConditionOfUseRepository(dataContext as ApplicationDbContext)},
                { typeof(IEventStatusRepository), (dataContext) =>  new EventStatusRepository(dataContext as ApplicationDbContext)},
                { typeof(IExpenseRepository), (dataContext) =>  new ExpenseRepository(dataContext as ApplicationDbContext)},
                { typeof(IExpenseStatusRepository), (dataContext) =>  new ExpenseStatusRepository(dataContext as ApplicationDbContext)},
                { typeof(IUsingEventRepository), (dataContext) =>  new UsingEventRepository(dataContext as ApplicationDbContext)},
                { typeof(ITranslationRepository), (dataContext) =>  new TranslationRepository(dataContext as ApplicationDbContext)},
                { typeof(IMultiLangStringRepository), (dataContext) =>  new MultiLangStringRepository(dataContext as ApplicationDbContext)},
                { typeof(IGroupRepository), (dataContext) => new GroupRepository(dataContext as ApplicationDbContext)},
                { typeof(IUserInGroupRepository), (dataContext) => new UserInGroupRepository(dataContext as ApplicationDbContext)},
                {typeof(ICarInGroupRepository), (dataContext) => new CarInGroupRepository(dataContext as ApplicationDbContext) }


            };
        }
    }
}
