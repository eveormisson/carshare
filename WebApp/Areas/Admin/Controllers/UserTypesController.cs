﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;
using DAL.App.Interfaces;
using WebApp.Areas.Admin.ViewModels;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]

    public class UserTypesController : Controller
    {
        
        private readonly IAppUnitOfWork _uow;

        public UserTypesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: UserTypes
        public async Task<IActionResult> Index()
        {
            // Multilang/translation Includes in repo.
            return View(await _uow.UserTypes.AllAsync());
        }

        // GET: UserTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the User Type in the url.");
            }

            var userType = await _uow.UserTypes.FindTranslationAsync(id.Value);
                
            if (userType == null)
            {
                return new NotFoundView($"The User Type with the Id: {id} is not found in the database.");
            }

            var vm = new UserTypeVM();
            vm.UserType = userType;

            return View(vm);
        }

        // GET: UserTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: UserTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UserTypeVM vm)
        {

            if (ModelState.IsValid)
            {
                vm.UserType.UserTypeName = new MultiLangString(vm.UserTypeName);

                await _uow.UserTypes.AddAsync(vm.UserType);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: UserTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the User Type in the url.");
            }

            // Multilang/translation Included in repo.
            var userType = await _uow.UserTypes.FindTranslationAsync(id.Value);

            if (userType == null)
            {
                return new NotFoundView($"The User Type with the Id: {id} is not found in the database.");
            }

            var vm = new UserTypeVM();

            //getting the translated value
            vm.UserTypeName = userType.UserTypeName.ToString();
            vm.UserType = userType;

            return View(vm);
        }

        // POST: UserTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UserTypeVM vm)
        {
            if (id != vm.UserType.UserTypeId)
            {
                return new NotFoundView($"The User Type with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    vm.UserType.UserTypeName = await _uow.MultiLangStrings.FindSingleAsync(vm.UserType.UserTypeNameId)
                        ?? new MultiLangString();


                    //Updating the multilangstring
                    vm.UserType.UserTypeName.SetTranslation(vm.UserTypeName);

                    _uow.UserTypes.Update(vm.UserType);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!( await _uow.UserTypes.ExistsByPrimaryKeyAsync(vm.UserType.UserTypeId)))
                    {
                        return new NotFoundView($"The User Type with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: UserTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the User Type in the url.");
            }

            var userType = await _uow.UserTypes.FindTranslationAsync(id.Value);
                
            if (userType == null)
            {
                return new NotFoundView($"The User Type with the Id: {id} is not found in the database.");
            }

            var vm = new UserTypeVM();
            vm.UserType = userType;

            return View(vm);
        }

        // POST: UserTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            _uow.UserTypes.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
