﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces.Repositories;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using Microsoft.AspNetCore.Authorization;
using WebApp.Areas.Admin.ViewModels;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CarStatusController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public CarStatusController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        
        // GET: CarStatus
        public async Task<IActionResult> Index()
        {
            return View(await _uow.CarStatus.AllAsync());
        }

        // GET: CarStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Car Status in the url.");
            }

            var carStatus = await _uow.CarStatus.FindTranslationAsync(id.Value);

            if (carStatus == null)
            {
                return new NotFoundView($"The Car Status with the Id: {id} is not found in the database.");
            }

            var vm = new CarStatusVM();
            vm.CarStatus = carStatus;

            return View(vm);
        }

        // GET: CarStatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CarStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CarStatusVM vm)
        {
            if (ModelState.IsValid)
            {
                vm.CarStatus.CarStatusName = new MultiLangString(vm.CarStatusName);

                await _uow.CarStatus.AddAsync(vm.CarStatus);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: CarStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Car Status in the url.");
            }

            var carStatus = await _uow.CarStatus.FindTranslationAsync(id.Value);

            if (carStatus == null)
            {
                return new NotFoundView($"The Car Status with the Id: {id} is not found in the database.");
            }

            var vm = new CarStatusVM();

            vm.CarStatusName = carStatus.CarStatusName.ToString();
            vm.CarStatus = carStatus;

            return View(vm);
        }

        // POST: CarStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CarStatusVM vm)
        {
            if (id != vm.CarStatus.CarStatusId)
            {
                return new NotFoundView($"The Car Status with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {


                    vm.CarStatus.CarStatusName = await _uow.MultiLangStrings.FindSingleAsync(vm.CarStatus.CarStatusNameId)
                        ?? new MultiLangString();

                    vm.CarStatus.CarStatusName.SetTranslation(vm.CarStatusName);

                    _uow.CarStatus.Update(vm.CarStatus);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! (await _uow.CarStatus.ExistsByPrimaryKeyAsync(vm.CarStatus.CarStatusId)))
                    {
                        return new NotFoundView($"The Car Status with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: CarStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Car Status in the url.");
            }

            var carStatus = await _uow.CarStatus.FindTranslationAsync(id.Value);

            if (carStatus == null)
            {
                return new NotFoundView($"The Car Status with the Id: {id} is not found in the database.");
            }

            var vm = new CarStatusVM();
            vm.CarStatus = carStatus;

            return View(vm);
        }

        // POST: CarStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _uow.CarStatus.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
