﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;
using WebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]

    public class ApplicationUsersController : Controller
    {
        
        private readonly IAppUnitOfWork _uow;

        public ApplicationUsersController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: ApplicationUsers
        public async Task<IActionResult> Index()
        {
            return View(await _uow.ApplicationUsers.AllAsync());
        }

        // GET: ApplicationUsers/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the User in the url.");
            }

            var applicationUser = await _uow.ApplicationUsers.FindAsync(id);
                
            if (applicationUser == null)
            {
                return new NotFoundView($"The User with the Id: {id} is not found in the database.");
            }

            return View(applicationUser);
        }

        // GET: ApplicationUsers/Create
        public IActionResult Create()
        {
            var vm = new ApplicationUsersCreateEditVM();

            return View(vm);
        }

        // POST: ApplicationUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ApplicationUsersCreateEditVM vm)
        {
            if (ModelState.IsValid)
            {
                await _uow.ApplicationUsers.AddAsync(vm.ApplicationUser);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: ApplicationUsers/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            var vm = new ApplicationUsersCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the User in the url.");
            }

            
            vm.ApplicationUser = await _uow.ApplicationUsers.FindByGuidAsync(id);
            

            if (vm.ApplicationUser == null)
            {
                return new NotFoundView($"The User with the Id: {id} is not found in the database.");
            }

        

            return View(vm);
        }

        // POST: ApplicationUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, ApplicationUsersCreateEditVM vm)
        {


            if (id != vm.ApplicationUser.Id)
            {
                return new NotFoundView($"The User with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.ApplicationUsers.Update(vm.ApplicationUser);
                    await _uow.SaveChangesAsync();
                }

                catch (DbUpdateConcurrencyException)
                {
                    if (!(await _uow.ApplicationUsers.ExistsByPrimaryKeyAsync(vm.ApplicationUser.Id)))
                    {
                        return new NotFoundView($"The User with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: ApplicationUsers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the user in the url.");
            }

            var applicationUser = await _uow.ApplicationUsers.FindAsync(id);
                
            if (applicationUser == null)
            {
                return new NotFoundView($"The User with the Id: {id} is not found in the database.");
            }

            return View(applicationUser);
        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
           
            _uow.ApplicationUsers.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
