﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;
using DAL.App.Interfaces;
using WebApp.Areas.Admin.ViewModels;
using WebApp.Helpers;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CarDetailsController : Controller
    {
        
        private readonly IAppUnitOfWork _uow;

        public CarDetailsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Admin/CarDetails
        public async Task<IActionResult> Index()
        {
            
            return View(await _uow.CarDetails.AllAsync());
        }

        // GET: Admin/CarDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Detail in the url.");
            }

            var carDetail = await _uow.CarDetails.FindWithUsersCarsAsync(id.Value);
                
            if (carDetail == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            return View(carDetail);
        }

        // GET: Admin/CarDetails/Create
        public IActionResult Create()
        {
            var vm = new CarDetailsCreateEditVM();
            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName");
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "ColourBrandLicencePlate");
           
            return View(vm);
        }

        // POST: Admin/CarDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CarDetailsCreateEditVM vm)
        {
            if (ModelState.IsValid)
            {
                await _uow.CarDetails.AddAsync(vm.CarDetail);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.CarDetail.ApplicationUserId);
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "ColourBrandLicencePlate", vm.CarDetail.CarId);
           
            return View(vm);
        }

        // GET: Admin/CarDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new CarDetailsCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Detail in the url.");
            }

            var carDetail = await _uow.CarDetails.FindAsync(id);
            vm.CarDetail = carDetail;

            if (carDetail == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.CarDetail.ApplicationUserId);
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "ColourBrandLicencePlate", vm.CarDetail.CarId);
    

            return View(vm);
        }

        // POST: Admin/CarDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CarDetailsCreateEditVM vm)
        {
            if (id != vm.CarDetail.CarDetailId)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.CarDetails.Update(vm.CarDetail);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await _uow.CarDetails.ExistsByPrimaryKeyAsync(vm.CarDetail.CarDetailId)))
                    {
                        return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.CarDetail.ApplicationUserId);
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "ColourBrandLicencePlate", vm.CarDetail.CarId);
            
            return View(vm);
        }

        // GET: Admin/CarDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            var carDetail = await _uow.CarDetails.FindWithUsersCarsAsync(id.Value);

            if (carDetail == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            return View(carDetail);
        }

        // POST: Admin/CarDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            _uow.CarDetails.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
