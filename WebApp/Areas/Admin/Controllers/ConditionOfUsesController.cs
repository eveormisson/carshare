﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;
using DAL.App.Interfaces;
using WebApp.Areas.Admin.ViewModels;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ConditionOfUsesController : Controller
    {
        
        private readonly IAppUnitOfWork _uow;


        public ConditionOfUsesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Admin/AdminConditionOfUses
        public async Task<IActionResult> Index()
        {
            
            return View(await _uow.ConditionOfUses.AllAsync());
        }

        // GET: Admin/AdminConditionOfUses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Condition in the url.");
            }

            var conditionOfUse = await _uow.ConditionOfUses.FindWithUsersCarsAsync(id.Value);
                
            if (conditionOfUse == null)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            return View(conditionOfUse);
        }

        // GET: Admin/AdminConditionOfUses/Create
        public IActionResult Create()
        {
            var vm = new ConditionOfUsesVM();

            vm.ApplicationUserSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName");
            vm.CarSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand");
            
            return View(vm);
        }

        // POST: Admin/AdminConditionOfUses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ConditionOfUsesVM vm)
        {
            var userId = User.Identity.GetUserId();
            vm.ConditionOfUse.ApplicationUserId = userId;

            if (ModelState.IsValid)
            {
                await _uow.ConditionOfUses.AddAsync(vm.ConditionOfUse);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.ApplicationUserSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.ConditionOfUse.ApplicationUser.FirstLastName);
            vm.CarSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand", vm.ConditionOfUse.Car.CarBrand);
            
            return View(vm);
        }

        // GET: Admin/AdminConditionOfUses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new ConditionOfUsesVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Condition in the url.");
            }

            vm.ConditionOfUse = await _uow.ConditionOfUses.FindAsync(id);

            

            if (vm.ConditionOfUse == null)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }
            vm.ApplicationUserSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.ConditionOfUse.ApplicationUser.FirstLastName);
            vm.CarSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand", vm.ConditionOfUse.Car.CarBrand);
            
            return View(vm);
        }

        // POST: Admin/AdminConditionOfUses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ConditionOfUsesVM vm)
        {
            if (id != vm.ConditionOfUse.ConditionOfUseId)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.ConditionOfUses.Update(vm.ConditionOfUse);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!( await _uow.ConditionOfUses.ExistsByPrimaryKeyAsync(vm.ConditionOfUse.ConditionOfUseId)))
                    {
                        return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            vm.ApplicationUserSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.ConditionOfUse.ApplicationUser.FirstLastName);
            vm.CarSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand", vm.ConditionOfUse.Car.CarBrand);
           
            return View(vm);
        }

        // GET: Admin/AdminConditionOfUses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Condition in the url.");
            }

            var conditionOfUse = await _uow.ConditionOfUses.FindWithUsersCarsAsync(id.Value);

            if (conditionOfUse == null)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            return View(conditionOfUse);
        }

        // POST: Admin/AdminConditionOfUses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            _uow.ConditionOfUses.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
