﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;
using DAL.App.Interfaces;
using WebApp.Areas.Admin.ViewModels;
using WebApp.Helpers;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ExpensesController : Controller
    {
       
        private readonly IAppUnitOfWork _uow;
        public ExpensesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Admin/Expenses
        public async Task<IActionResult> Index()
        {
            
            return View(await _uow.Expenses.AllAsync());
        }

        // GET: Admin/Expenses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense in the url.");
            }

            var expense = await _uow.Expenses.FindWithUsersCarsStatusesAsync(id.Value);
                
            if (expense == null)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            return View(expense);
        }

        // GET: Admin/Expenses/Create
        public IActionResult Create()
        {
            var vm = new ExpensesCreateEditVM();

            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName");
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand");
            vm.ExpenseStatusSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusName");


            return View(vm);
        }

        // POST: Admin/Expenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExpensesCreateEditVM vm)
        {
            if (ModelState.IsValid)
            {
                await _uow.Expenses.AddAsync(vm.Expense);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.Expense.ApplicationUserId);
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand", vm.Expense.CarId);
            vm.ExpenseStatusSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusName", vm.Expense.ExpenseStatusId);
            
            return View(vm);
        }

        // GET: Admin/Expenses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new ExpensesCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense in the url.");
            }

            var expense = await _uow.Expenses.FindAsync(id);

            vm.Expense = expense;

            if (expense == null)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", expense.ApplicationUserId);
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand", expense.CarId);
            vm.ExpenseStatusSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusName", expense.ExpenseStatusId);
            
            return View(vm);
        }

        // POST: Admin/Expenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ExpensesCreateEditVM vm)
        {
            if (id != vm.Expense.ExpenseId)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Expenses.Update(vm.Expense);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await _uow.Expenses.ExistsByPrimaryKeyAsync(vm.Expense.ExpenseId)))
                    {
                        return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.Expense.ApplicationUserId);
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarId", vm.Expense.CarId);
            vm.ExpenseStatusSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusId", vm.Expense.ExpenseStatusId);
           
            return View(vm);
        }

        // GET: Admin/Expenses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense in the url.");
            }

            var expense = await _uow.Expenses.FindWithUsersCarsStatusesAsync(id.Value);
                
            if (expense == null)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            return View(expense);
        }

        // POST: Admin/Expenses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            _uow.Expenses.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
