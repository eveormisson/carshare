﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces.Repositories;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using Microsoft.AspNetCore.Authorization;
using WebApp.Areas.Admin.ViewModels;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ExpenseStatusController : Controller
    {

        private readonly IAppUnitOfWork _uow;

        public ExpenseStatusController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: ExpenseStatus
        public async Task<IActionResult> Index()
        {
            // Multilang/translation Includes in repo.
            return View(await _uow.ExpenseStatus.AllAsync());
        }

        // GET: ExpenseStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense Status in the url.");
            }

            var expenseStatus = await _uow.ExpenseStatus.FindTranslationAsync(id.Value);

            if (expenseStatus == null)
            {
                return new NotFoundView($"The Expense Status with the Id: {id} is not found in the database.");
            }

            var vm = new ExpenseStatusVM();
            vm.ExpenseStatus = expenseStatus;

            return View(vm);
        }

        // GET: ExpenseStatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ExpenseStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExpenseStatusVM vm)
        {
            if (ModelState.IsValid)
            {
                vm.ExpenseStatus.ExpenseStatusName = new MultiLangString(vm.ExpenseStatusName);

                await _uow.ExpenseStatus.AddAsync(vm.ExpenseStatus);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: ExpenseStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense Status in the url.");
            }

            var expenseStatus = await _uow.ExpenseStatus.FindTranslationAsync(id.Value);

            if (expenseStatus == null)
            {
                return new NotFoundView($"The Expense Status with the Id: {id} is not found in the database.");
            }

            var vm = new ExpenseStatusVM();

            vm.ExpenseStatusName = expenseStatus.ExpenseStatusName.ToString();
            vm.ExpenseStatus = expenseStatus;
            
            return View(vm);
        }

        // POST: ExpenseStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ExpenseStatusVM vm)
        {
            if (id != vm.ExpenseStatus.ExpenseStatusId)
            {
                return new NotFoundView($"The Expense Status with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    vm.ExpenseStatus.ExpenseStatusName = await _uow.MultiLangStrings.FindSingleAsync(vm.ExpenseStatus.ExpenseStatusNameId)
                        ?? new MultiLangString();

                    vm.ExpenseStatus.ExpenseStatusName.SetTranslation(vm.ExpenseStatusName);
                 
                    _uow.ExpenseStatus.Update(vm.ExpenseStatus);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! (await _uow.ExpenseStatus.ExistsByPrimaryKeyAsync(vm.ExpenseStatus.ExpenseStatusId)))
                    {
                        return new NotFoundView($"The Expense Status with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: ExpenseStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense Status in the url.");
            }

            var expenseStatus = await _uow.ExpenseStatus.FindTranslationAsync(id.Value);

            if (expenseStatus == null)
            {
                return new NotFoundView($"The Expense Status with the Id: {id} is not found in the database.");
            }

            var vm = new ExpenseStatusVM();
            vm.ExpenseStatus = expenseStatus;

            return View(vm);
        }

        // POST: ExpenseStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _uow.ExpenseStatus.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
