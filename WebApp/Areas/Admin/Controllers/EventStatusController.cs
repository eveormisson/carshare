﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces.Repositories;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using Microsoft.AspNetCore.Authorization;
using WebApp.Areas.Admin.ViewModels;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class EventStatusController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public EventStatusController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: EventStatus
        public async Task<IActionResult> Index()
        {
            return View(await _uow.EventStatus.AllAsync());
        }

        // GET: EventStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event Status in the url.");
            }

            var eventStatus = await _uow.EventStatus.FindTranslationAsync(id.Value);

            if (eventStatus == null)
            {
                return new NotFoundView($"The Event Status with the Id: {id} is not found in the database.");
            }

            var vm = new EventStatusVM();
            vm.EventStatus = eventStatus;

            return View(vm);
        }

        // GET: EventStatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EventStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EventStatusVM vm)
        {
            if (ModelState.IsValid)
            {
                vm.EventStatus.EventStatusName = new MultiLangString(vm.EventStatusName);

                await _uow.EventStatus.AddAsync(vm.EventStatus);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: EventStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event Status in the url.");
            }

            var eventStatus = await _uow.EventStatus.FindTranslationAsync(id.Value);

            if (eventStatus == null)
            {
                return new NotFoundView($"The Event Status with the Id: {id} is not found in the database.");
            }

            var vm = new EventStatusVM();

            vm.EventStatusName = eventStatus.EventStatusName.ToString();
            vm.EventStatus = eventStatus;
           
            return View(vm);
        }

        // POST: EventStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EventStatusVM vm)
        {
            if (id != vm.EventStatus.EventStatusId)
            {
                return new NotFoundView($"The Event Status with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    vm.EventStatus.EventStatusName = await _uow.MultiLangStrings.FindSingleAsync(vm.EventStatus.EventStatusNameId)
                        ?? new MultiLangString();

                    vm.EventStatus.EventStatusName.SetTranslation(vm.EventStatusName);
                    
                    _uow.EventStatus.Update(vm.EventStatus);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! (await _uow.EventStatus.ExistsByPrimaryKeyAsync(vm.EventStatus.EventStatusId)))
                    {
                        return new NotFoundView($"The Event Status with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: EventStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event Status in the url.");
            }

            var eventStatus = await _uow.EventStatus.FindTranslationAsync(id.Value);

            if (eventStatus == null)
            {
                return new NotFoundView($"The Event Status with the Id: {id} is not found in the database.");
            }

            var vm = new EventStatusVM();
            vm.EventStatus = eventStatus;

            return View(vm);
        }

        // POST: EventStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _uow.EventStatus.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
