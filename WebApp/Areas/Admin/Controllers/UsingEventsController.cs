﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;
using WebApp.Areas.Admin.ViewModels;
using Microsoft.AspNetCore.Authorization;
using WebApp.Helpers;

namespace WebApp.Areas.Admin.Controllers
{

    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UsingEventsController : Controller
    {
        
        private readonly IAppUnitOfWork _uow;


        public UsingEventsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Admin/AdminUsingEvents
        public async Task<IActionResult> Index()
        {
            
            return View(await _uow.UsingEvents.AllAsync());
        }

        // GET: Admin/AdminUsingEvents/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event in the url.");
            }

            var usingEvent = await _uow.UsingEvents.FindWithUsersCarsStatusesAsync(id.Value);
               
                

            if (usingEvent == null)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }

            return View(usingEvent);
        }

        // GET: Admin/AdminUsingEvents/Create
        public IActionResult Create()
        {
            var vm = new UsingEventsCreateEditVM();

            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand");
            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName");
            vm.EventStatusSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusName");

            return View(vm);
        }

        // POST: Admin/AdminUsingEvents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UsingEventsCreateEditVM vm)
        {
            if (ModelState.IsValid)
            {
                await _uow.UsingEvents.AddAsync(vm.UsingEvent);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand");
            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName");
            vm.EventStatusSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusName");

            return View(vm);
        }

        // GET: Admin/AdminUsingEvents/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new UsingEventsCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event in the url.");
            }

            vm.UsingEvent = await _uow.UsingEvents.FindAsync(id);

            

            if (vm.UsingEvent == null)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }
            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand");
            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName");
            vm.EventStatusSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusName");
            
            return View(vm);
        }

        // POST: Admin/AdminUsingEvents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UsingEventsCreateEditVM vm)
        {
            if (id != vm.UsingEvent.UsingEventId)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.UsingEvents.Update(vm.UsingEvent);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!( await _uow.UsingEvents.ExistsByPrimaryKeyAsync(vm.UsingEvent.UsingEventId)))
                    {
                        return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            vm.CarsSelectList = new SelectList(_uow.Cars.All(), "CarId", "CarBrand");
            vm.ApplicationUsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName");
            vm.EventStatusSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusName");

            return View(vm);
        }

        // GET: Admin/AdminUsingEvents/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event in the url.");
            }

            var usingEvent = await _uow.UsingEvents.FindWithUsersCarsStatusesAsync(id.Value);


            if (usingEvent == null)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }

            return View(usingEvent);
        }

        // POST: Admin/AdminUsingEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            _uow.UsingEvents.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
