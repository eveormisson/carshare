﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.ViewModels
{
    public class ConditionOfUsesVM
    {
        public ConditionOfUse ConditionOfUse { get; set; }

        public SelectList CarSelectList { get; set; }

        public SelectList ApplicationUserSelectList { get; set; }
    }
}
