﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.ViewModels
{
    public class CarDetailsCreateEditVM
    {
        public CarDetail CarDetail { get; set; }

        public SelectList ApplicationUsersSelectList { get; set; }

        public SelectList CarsSelectList { get; set; }
    }
}
