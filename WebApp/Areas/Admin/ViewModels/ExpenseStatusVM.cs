﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.ViewModels
{
    public class ExpenseStatusVM
    {
        public ExpenseStatus ExpenseStatus { get; set; } = new ExpenseStatus();

        [MaxLength(25)]
        public string ExpenseStatusName { get; set; }
    }
}
