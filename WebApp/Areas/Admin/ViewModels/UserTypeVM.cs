﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.ViewModels
{
    public class UserTypeVM
    {
        public UserType UserType { get; set; } = new UserType();

        [MaxLength(25)]
        public string UserTypeName { get; set; }
    }
}
