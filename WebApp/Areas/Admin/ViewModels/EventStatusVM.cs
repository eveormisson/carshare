﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.ViewModels
{
    public class EventStatusVM
    {
        public EventStatus EventStatus { get; set; } = new EventStatus();

        [MaxLength(25)]
        public string EventStatusName { get; set; }

    }
}
