﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.ViewModels
{
    public class CarStatusVM
    {

        public CarStatus CarStatus { get; set; } = new CarStatus();

        [MaxLength(25)]
        public string CarStatusName { get; set; }
    }
}
