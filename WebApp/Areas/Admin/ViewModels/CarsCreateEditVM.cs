﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.ViewModels
{
    public class CarsCreateEditVM
    {
        public Car Car { get; set; }

        public SelectList CarStatusesSelectList { get; set; }

        public SelectList ApplicationUsersSelectList { get; set; }

    }
}
