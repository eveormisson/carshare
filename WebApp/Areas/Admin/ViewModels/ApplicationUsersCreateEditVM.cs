﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class ApplicationUsersCreateEditVM
    {
        public ApplicationUser ApplicationUser { get; set; }
    }
}
