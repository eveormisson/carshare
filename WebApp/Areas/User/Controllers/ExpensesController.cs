﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces.Repositories;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using WebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "User")]
    [Area("User")]
    public class ExpensesController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public ExpensesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Expenses
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            return View(await _uow.Expenses.AllBelongingToUserAsync(userId));
        }

        // GET: Expenses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense in the url.");
            }

            var userId = User.Identity.GetUserId();
            var expense = await _uow.Expenses.GetBelongingToUserWithStatusCarAsync(id.Value, userId);

            if (expense == null)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            return View(expense);
        }

        // GET: Expenses/Create
        public async Task<IActionResult> Create()
        {
            var vm = new ExpensesCreateEditVM();
            var userId = User.Identity.GetUserId();

            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand");
            vm.ExpenseStatusesSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusName");

            return View(vm);
        }

        // POST: Expenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExpensesCreateEditVM vm)
        {
            var userId = User.Identity.GetUserId();
            vm.Expense.ApplicationUserId = userId;

            TryValidateModel(vm);
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                await _uow.Expenses.AddAsync(vm.Expense);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.Expense.ApplicationUserId = userId;
            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.Expense.CarId);
            vm.ExpenseStatusesSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusName", vm.Expense.ExpenseStatusId);
            
            return View(vm);
        }

        // GET: Expenses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new ExpensesCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense in the url.");
            }

            var userId = User.Identity.GetUserId();

            vm.Expense = await _uow.Expenses.GetBelongingToUserAsync(id.Value, userId);

            if (vm.Expense == null)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            vm.Expense.ApplicationUserId = userId;
            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.Expense.CarId);
            vm.ExpenseStatusesSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusName", vm.Expense.ExpenseStatusId);
            
            return View(vm);
        }

        // POST: Expenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ExpensesCreateEditVM vm)
        {
            if (id != vm.Expense.ExpenseId)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            var userId = User.Identity.GetUserId();

            var dbExpense = await _uow.Expenses.GetBelongingToUserAsync(vm.Expense.ExpenseId, userId);

            if (dbExpense == null)
            {
                return BadRequest();
            }

            vm.Expense.ApplicationUserId = userId;

            if (ModelState.IsValid)
            {
                dbExpense.ExpenseId = vm.Expense.ExpenseId;
                dbExpense.ApplicationUserId = vm.Expense.ApplicationUserId;
                dbExpense.CarId = vm.Expense.CarId;
                dbExpense.ExpenseDescription = vm.Expense.ExpenseDescription;
                dbExpense.ExpenseName = vm.Expense.ExpenseName;
                dbExpense.ExpenseStatusId = vm.Expense.ExpenseStatusId;
                dbExpense.ExpenseTotal = vm.Expense.ExpenseTotal;
                dbExpense.ApplicationUserId = vm.Expense.ApplicationUserId;

                try
                {
                    _uow.Expenses.Update(dbExpense);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! (await _uow.Expenses.ExistsByPrimaryKeyAsync(vm.Expense.ExpenseId)))
                    {
                        return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            vm.Expense.ApplicationUserId = userId;
            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.Expense.CarId);
            vm.ExpenseStatusesSelectList = new SelectList(_uow.ExpenseStatus.All(), "ExpenseStatusId", "ExpenseStatusName", vm.Expense.ExpenseStatusId);
           
            return View(vm);
        }

        // GET: Expenses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Expense in the url.");
            }

            var userId = User.Identity.GetUserId();
            var expense = await _uow.Expenses.GetBelongingToUserWithStatusCarAsync(id.Value, userId);

            if (expense == null)
            {
                return new NotFoundView($"The Expense with the Id: {id} is not found in the database.");
            }

            return View(expense);
        }

        // POST: Expenses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _uow.Expenses.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
