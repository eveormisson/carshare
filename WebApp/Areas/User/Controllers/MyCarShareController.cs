﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.EF;
using DAL.App.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Areas.User.ViewModels;

namespace WebApp.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [Area("User")]
    public class MyCarShareController : Controller
    {
        private readonly IAppUnitOfWork _uow;
       

        public MyCarShareController(IAppUnitOfWork uow)
        {
            _uow = uow;
           
        }

        public async Task<IActionResult> Index()
        {
            var vm = new MyCarShareVM();

            var userId = User.Identity.GetUserId();

            vm.UserCars = await _uow.Cars.AllBelongingToUserAsync(userId);
            vm.UserEvents = await _uow.UsingEvents.AllBelongingToUserAsync(userId);
            
            vm.Friends = _uow.ApplicationUsers.GetAllApplicationUsers();
           

            return View(vm);
           
        }
    }
}