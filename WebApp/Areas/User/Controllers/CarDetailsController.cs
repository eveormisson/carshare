﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces.Repositories;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using WebApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "User")]
    [Area("User")]
   
    public class CarDetailsController : Controller
    {
       
        private readonly IAppUnitOfWork _uow;

        public CarDetailsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: CarDetails
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            return View(await _uow.CarDetails.AllBelongingToUserAsync(userId));
        }

        // GET: CarDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Detail in the url.");
            }

            var userId = User.Identity.GetUserId();
            var carDetail = await _uow.CarDetails.GetBelongingToUserWithCarAsync(id.Value, userId);

            if (carDetail == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            return View(carDetail);
        }

        // GET: CarDetails/Create
        public async Task<IActionResult> Create()
        {
            var vm = new CarDetailsCreateEditVM();
            var userId = User.Identity.GetUserId();

            vm.CarSelectlist = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand");

            return View(vm);
        }

        // POST: CarDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CarDetailsCreateEditVM vm)
        {
            var userId = User.Identity.GetUserId();
            vm.CarDetail.ApplicationUserId = userId;

            TryValidateModel(vm);
            ModelState.Clear();

 

            if (ModelState.IsValid)
            {
                
                await _uow.CarDetails.AddAsync(vm.CarDetail);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            
            vm.CarSelectlist = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.CarDetail.CarId);

            return View(vm);
        }

        // GET: CarDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new CarDetailsCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Detail in the url.");
            }

            var userId = User.Identity.GetUserId();
            var carDetail = await _uow.CarDetails.GetBelongingToUserAsync(id.Value, userId);

            if (carDetail == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            vm.CarDetail = carDetail;
            
            vm.CarSelectlist = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.CarDetail.CarId);
            
            return View(vm);
        }

        // POST: CarDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CarDetailsCreateEditVM vm)
        {
            if (id != vm.CarDetail.CarDetailId)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            var userId = User.Identity.GetUserId();

            var dbCarDetail = await _uow.CarDetails.GetBelongingToUserAsync(vm.CarDetail.CarDetailId, userId);

            if (dbCarDetail == null)
            {
                return BadRequest();
            }

            vm.CarDetail.ApplicationUserId = userId;

            if (ModelState.IsValid)
            {
                dbCarDetail.CarDetailDescription = vm.CarDetail.CarDetailDescription;
                dbCarDetail.CarDetailName = vm.CarDetail.CarDetailName;
                dbCarDetail.ApplicationUserId = vm.CarDetail.ApplicationUserId;
                dbCarDetail.CarId = vm.CarDetail.CarId;
                
               
                try
                {
                    _uow.CarDetails.Update(dbCarDetail);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! (await _uow.CarDetails.ExistsByPrimaryKeyAsync(vm.CarDetail.CarDetailId)))
                    {
                        return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            vm.CarSelectlist = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.CarDetail.CarId);
            return View(vm);
        }

        // GET: CarDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Detail in the url.");
            }

            var userId = User.Identity.GetUserId();
            var carDetail = await _uow.CarDetails.GetBelongingToUserWithCarAsync(id.Value, userId);

            if (carDetail == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            return View(carDetail);
        }

        // POST: CarDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userId = User.Identity.GetUserId();
            var carDetail = await _uow.CarDetails.GetBelongingToUserAsync(id, userId);

            if (carDetail == null)
            {
                return new NotFoundView($"The Detail with the Id: {id} is not found in the database.");
            }

            _uow.CarDetails.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

       
    }
}
