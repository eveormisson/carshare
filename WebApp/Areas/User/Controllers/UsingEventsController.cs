﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;
using WebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "User")]
    [Area("User")]
    public class UsingEventsController : Controller
    {
        
        private readonly IAppUnitOfWork _uow;

        public UsingEventsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: UsingEvents
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            return View(await _uow.UsingEvents.AllBelongingToUserAsync(userId));
        }

        // GET: UsingEvents/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event in the url.");
            }

            var userId = User.Identity.GetUserId();
            var usingEvent = await _uow.UsingEvents.GetBelongingToUserWithCarsUsersStatusesAsync(id.Value, userId);

                
            if (usingEvent == null)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }

            return View(usingEvent);
        }

        // GET: UsingEvents/Create
        public async  Task<IActionResult> Create()
        {
            var vm = new UsingEventsCreateEditVM();
            var userId = User.Identity.GetUserId();

            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand");
            vm.EventStatusesSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusName");

            return View(vm);
        }

        // POST: UsingEvents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UsingEventsCreateEditVM vm)
        {
            var userId = User.Identity.GetUserId();
            vm.UsingEvent.ApplicationUserId = userId;

            TryValidateModel(vm);
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                await _uow.UsingEvents.AddAsync(vm.UsingEvent);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.UsingEvent.CarId);
            vm.EventStatusesSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusName", vm.UsingEvent.EventStatusId);

            return View(vm);
        }

        // GET: UsingEvents/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new UsingEventsCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event in the url.");
            }

            var userId = User.Identity.GetUserId();

            var usingEvent = await _uow.UsingEvents.GetBelongingToUserAsync(id.Value, userId);

            if (usingEvent == null)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }

            vm.UsingEvent = usingEvent;

            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.UsingEvent.CarId);
            vm.EventStatusesSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusName", usingEvent.EventStatusId);


            return View(vm);
        }

        // POST: UsingEvents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UsingEventsCreateEditVM vm)
        {

            if (id != vm.UsingEvent.UsingEventId)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }

            var userId = User.Identity.GetUserId();

            var dbUsingEvent = await _uow.UsingEvents.GetBelongingToUserAsync(vm.UsingEvent.UsingEventId, userId);

            if (dbUsingEvent == null)
            {
                return BadRequest();
            }

            vm.UsingEvent.ApplicationUserId = userId;

           
            if (ModelState.IsValid)
            {
               
                
                dbUsingEvent.DrivingDestination = vm.UsingEvent.DrivingDestination;
                dbUsingEvent.EventStartTime = vm.UsingEvent.EventStartTime;
                dbUsingEvent.EventEndTime = vm.UsingEvent.EventEndTime;
                dbUsingEvent.EventDescription = vm.UsingEvent.EventDescription;
                dbUsingEvent.CarId = vm.UsingEvent.CarId;
                dbUsingEvent.EventStatusId = vm.UsingEvent.EventStatusId;
                dbUsingEvent.ApplicationUserId = vm.UsingEvent.ApplicationUserId;
                

                try
                {
                    _uow.UsingEvents.Update(dbUsingEvent);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await _uow.UsingEvents.ExistsByPrimaryKeyAsync(vm.UsingEvent.UsingEventId)))
                    {
                        return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            vm.CarsSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", vm.UsingEvent.CarId);
            vm.EventStatusesSelectList = new SelectList(_uow.EventStatus.All(), "EventStatusId", "EventStatusId", vm.UsingEvent.EventStatusId);
        

            return View(vm);
        }

        // GET: UsingEvents/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Event in the url.");
            }

            var userId = User.Identity.GetUserId();
            var usingEvent = await _uow.UsingEvents.GetBelongingToUserWithCarsUsersStatusesAsync(id.Value, userId);

            if (usingEvent == null)
            {
                return new NotFoundView($"The Event with the Id: {id} is not found in the database.");
            }

            return View(usingEvent);
        }

        // POST: UsingEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userId = User.Identity.GetUserId();
            var usingEvent = await _uow.UsingEvents.GetBelongingToUserAsync(id, userId);

            if (usingEvent == null)
            {
                return BadRequest();
            }

            _uow.UsingEvents.Remove(usingEvent);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
