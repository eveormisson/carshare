﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces.Repositories;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using WebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "User")]
    [Area("User")]
    public class ConditionOfUsesController : Controller
    {
        
        private readonly IAppUnitOfWork _uow;

        public ConditionOfUsesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: ConditionOfUses
        public async Task<IActionResult> Index()
        {

            var userId = User.Identity.GetUserId();
            return View(await _uow.ConditionOfUses.AllBelongingToUserAsync(userId));
            
        }

        // GET: ConditionOfUses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Condition in the url.");
            }

            var userId = User.Identity.GetUserId();
            var conditionOfUse = await _uow.ConditionOfUses.GetBelongingToUserWithCarAsync(id.Value, userId);

            if (conditionOfUse == null)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            return View(conditionOfUse);
        }

        // GET: ConditionOfUses/Create
        public async Task<IActionResult> Create()
        {
            var vm = new ConditionOfUseCreateEditVM();
            var userId = User.Identity.GetUserId();

            vm.CarSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand");

            return View(vm);
        }

        // POST: ConditionOfUses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ConditionOfUseCreateEditVM vm)
        {

            var userId = User.Identity.GetUserId();
            vm.ConditionOfUse.ApplicationUserId = userId;

            TryValidateModel(vm);
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                await _uow.ConditionOfUses.AddAsync(vm.ConditionOfUse);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.CarSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarId", vm.ConditionOfUse.CarId);
            
            return View(vm);
        }

        // GET: ConditionOfUses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new ConditionOfUseCreateEditVM();

            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Condition in the url.");
            }

            var userId = User.Identity.GetUserId();

            var conditionOfUse = await _uow.ConditionOfUses.GetBelongingToUserAsync(id.Value, userId);

            if (conditionOfUse == null)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            vm.ConditionOfUse = conditionOfUse;
            
            vm.CarSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarBrand", conditionOfUse.CarId);
            return View(vm);
        }

        // POST: ConditionOfUses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ConditionOfUseCreateEditVM vm)
        {

            if (id != vm.ConditionOfUse.ConditionOfUseId)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            var userId = User.Identity.GetUserId();

            //check the id just in case
            var dbCondition = await _uow.ConditionOfUses.GetBelongingToUserAsync(vm.ConditionOfUse.ConditionOfUseId, userId);


            if (dbCondition == null)
            {
                return BadRequest();
            }

            //put back the user id 
            //otherwise the record is lost!
            vm.ConditionOfUse.ApplicationUserId = userId;


            if (ModelState.IsValid)
            {
                dbCondition.ConditionOfUseDescription = vm.ConditionOfUse.ConditionOfUseDescription;
                dbCondition.ConditionOfUseName = vm.ConditionOfUse.ConditionOfUseName;
                dbCondition.CarId = vm.ConditionOfUse.CarId;
                dbCondition.ApplicationUserId = vm.ConditionOfUse.ApplicationUserId;
                

                try
                {
                    _uow.ConditionOfUses.Update(dbCondition);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! (await _uow.ConditionOfUses.ExistsByPrimaryKeyAsync(vm.ConditionOfUse.ConditionOfUseId)))
                    {
                        return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            vm.CarSelectList = new SelectList(await _uow.Cars.AllBelongingToUserAsync(userId), "CarId", "CarId", vm.ConditionOfUse.CarId);
            return View(vm);
        }

        // GET: ConditionOfUses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Condition in the url.");
            }

            var userId = User.Identity.GetUserId();
            var conditionOfUse = await _uow.ConditionOfUses.GetBelongingToUserWithCarAsync(id.Value, userId);

            if (conditionOfUse == null)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            return View(conditionOfUse);
        }

        // POST: ConditionOfUses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userId = User.Identity.GetUserId();
            var condition = await _uow.ConditionOfUses.GetBelongingToUserAsync(id, userId);

            if (condition == null)
            {
                return new NotFoundView($"The Condition with the Id: {id} is not found in the database.");
            }

            _uow.ConditionOfUses.Remove(condition);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
