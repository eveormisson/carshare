﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;
using WebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    [Authorize(Roles = "User")]
    [Area("User")]
    public class CarsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public CarsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Cars
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            return View(await _uow.Cars.AllBelongingToUserAsync(userId));
        }

        // GET: Cars/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Car in the url.");
            }

            var userId = User.Identity.GetUserId();
            var car = await _uow.Cars.GetBelongingToUserWithStatusUserAsync(id.Value, userId);

            if (car == null)
            {
                return new NotFoundView($"The Car with the Id: {id} is not found in the database.");
            }

            return View(car);
        }

        // GET: Cars/Create
        public IActionResult Create()
        {
            var vm = new CarCreateEditVM();
            vm.CarStatusesSelectList = new SelectList(_uow.CarStatus.All(), "CarStatusId", "CarStatusName");
            return View(vm);
        }

        // POST: Cars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CarCreateEditVM vm)
        {
            //valdiate the model later, because AppicationUserId is initailly null

            var userId = User.Identity.GetUserId();
            vm.Car.OwnerId = userId;

            TryValidateModel(vm);
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                await _uow.Cars.AddAsync(vm.Car);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.CarStatusesSelectList = new SelectList(_uow.CarStatus.All(), "CarStatusId", "CarStatusName", vm.Car.CarStatusId);

            return View(vm);
        }

        // GET: Cars/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var vm = new CarCreateEditVM();
           
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Car in the url.");
            }

            var userId = User.Identity.GetUserId();

            var car = await _uow.Cars.GetBelongingToUserAsync(id.Value, userId);

            if (car == null)
            {
                return new NotFoundView($"The Car with the Id: {id} is not found in the database.");
            }

            vm.Car = car;
            vm.CarStatusesSelectList = new SelectList(_uow.CarStatus.All(), "CarStatusId", "CarStatusName", car.CarStatusId);

            return View(vm);
        }

        // POST: Cars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CarCreateEditVM vm)
        {
            if (id != vm.Car.CarId)
            {
                return new NotFoundView($"The Car with the Id: {id} is not found in the database.");
            }

            var userId = User.Identity.GetUserId();

            //check the car id just in case
            var dbCar = await _uow.Cars.GetBelongingToUserAsync(vm.Car.CarId, userId);
            

            if (dbCar == null)
            {
                return BadRequest();
            }

            //put back the user id 
            //otherwise the record is lost!
            vm.Car.OwnerId = userId;
            

            if (ModelState.IsValid)
            {

                dbCar.CarBrand = vm.Car.CarBrand;
                dbCar.CarColour = vm.Car.CarColour;
                dbCar.CarLicensePlate = vm.Car.CarLicensePlate;
                dbCar.CarStatusId = vm.Car.CarStatusId;
                dbCar.CarYearOfIssue = vm.Car.CarYearOfIssue;
                dbCar.OwnerId = vm.Car.OwnerId;
                
              

                try
                {
                    _uow.Cars.Update(dbCar);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await _uow.Cars.ExistsByPrimaryKeyAsync(vm.Car.CarId)))
                    {
                        return new NotFoundView($"The Car with the Id: {id} is not found in the database.");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            vm.CarStatusesSelectList = new SelectList(_uow.CarStatus.All(), "CarStatusId", "CarStatusId", vm.Car.CarStatusId);
            
            
            return View(vm);
        }

        // GET: Cars/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundView($"Please include the ID of the Car in the url.");
            }

            var userId = User.Identity.GetUserId();
            var car = await _uow.Cars.GetBelongingToUserWithStatusUserAsync(id.Value, userId);

            if (car == null)
            {
                return new NotFoundView($"The Car with the Id: {id} is not found in the database.");
            }

            return View(car);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userId = User.Identity.GetUserId();
            var car = await _uow.Cars.GetBelongingToUserAsync(id, userId);

            if (car == null)
            {
                return new NotFoundView($"The Car with the Id: {id} is not found in the database.");
            }

            _uow.Cars.Remove(car);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Cars/AddUser
        public IActionResult AddUser(int carId)
        {
            var vm = new CarCreateEditVM();

           

            return View(vm);
        }

        // POST: Cars/AddUser
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUser(int carId, CarCreateEditVM vm, string userId)
        {

            vm.User = await _uow.ApplicationUsers.FindAsync(userId);
            vm.Car = await _uow.Cars.FindAsync(carId);

           

            TryValidateModel(vm);
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                _uow.UserTypesInCars.AddUserToCar(vm.User.Id, vm.Car.CarId);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            vm.UsersSelectList = new SelectList(_uow.ApplicationUsers.All(), "Id", "FirstLastName", vm.User.Id);
            return View(vm);
        }
    }
}
