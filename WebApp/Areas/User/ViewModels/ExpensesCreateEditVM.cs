﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class ExpensesCreateEditVM
    {
        public Expense Expense { get; set; }

        public SelectList CarsSelectList { get; set; }

        public SelectList ExpenseStatusesSelectList { get; set; }
    }
}
