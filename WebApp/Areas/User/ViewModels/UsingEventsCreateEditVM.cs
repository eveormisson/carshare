﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class UsingEventsCreateEditVM
    {
        public UsingEvent UsingEvent { get; set; }

        public SelectList CarsSelectList { get; set; }

        public SelectList EventStatusesSelectList { get; set; }
    }
}
