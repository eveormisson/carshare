﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class CarCreateEditVM
    {
        public Car Car { get; set; }

        public SelectList CarStatusesSelectList { get; set; }

        public SelectList OwnerSelectList { get; set; }

        public SelectList UsersSelectList { get; set;  }

        public ApplicationUser User { get; set; }
    }
}
