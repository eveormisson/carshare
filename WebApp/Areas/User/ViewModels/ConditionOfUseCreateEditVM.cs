﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class ConditionOfUseCreateEditVM
    {
        public ConditionOfUse ConditionOfUse { get; set; }

        public SelectList CarSelectList { get; set; }
    }
}
