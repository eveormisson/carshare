﻿using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.User.ViewModels
{
    public class MyCarShareVM
    {
        public ApplicationUser ApplictaionUser { get; set; }

        public Car Car { get; set; }

        public UsingEvent Event { get; set; }

        public SelectList ApplicationUsersSelectList { get; set;  }

        public SelectList CarsSelectList { get; set; }

        public List<ApplicationUser> Friends { get; set; }

        public List<Car> UserCars { get; set; }

        public List<UsingEvent> UserEvents { get; set; }

    }
}
