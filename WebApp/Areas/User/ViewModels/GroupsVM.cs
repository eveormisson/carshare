﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.User.ViewModels
{
    public class GroupsVM
    {
        public ApplicationUser ApplicationUser { get; set; }

        public List<ApplicationUser> ApplicationUsers { get; set; }

        public Group Group { get; set; }
    }
}
