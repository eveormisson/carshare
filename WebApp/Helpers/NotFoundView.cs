﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace WebApp.Helpers
{
    public class NotFoundView : ViewResult
    {
        public NotFoundView(string message)
        {
            ViewName = "NotFound";
            StatusCode = (int)HttpStatusCode.NotFound;
            ViewData = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary());
            ViewData.Model = message;
        }
    }
}
