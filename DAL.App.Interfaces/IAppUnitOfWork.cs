﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
       
        IUserTypeRepository UserTypes { get; }
        IApplicationUserRepository ApplicationUsers { get; }
        IUserTypeInCarRepository UserTypesInCars { get; }
        ICarRepository Cars { get; }
        ICarDetailRepository CarDetails { get; }
        ICarStatusRepository CarStatus { get; }
        IConditionOfUseRepository ConditionOfUses { get; }
        IEventStatusRepository EventStatus { get; }
        IExpenseRepository Expenses { get; }
        IExpenseStatusRepository ExpenseStatus { get; }
        IUsingEventRepository UsingEvents { get;  }
        IMultiLangStringRepository MultiLangStrings { get; }
        ITranslationRepository Translations { get; }
        IGroupRepository Groups { get; }
        IUserInGroupRepository UsersInGroup { get; }
        ICarInGroupRepository CarsInGroup { get; }
    }
}
