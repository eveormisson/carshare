﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface ICarDetailRepository : IRepository<CarDetail>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        Task<List<CarDetail>> AllBelongingToCarAsync(int carId);

        Task<CarDetail> GetBelongingToUserAsync(int carDetailId, string userId);

        Task<List<CarDetail>> AllBelongingToUserAsync(string userId);

        Task<CarDetail> FindWithUsersCarsAsync(int id);

        Task<CarDetail> GetBelongingToUserWithCarAsync(int carDetailId, string userId);
    }
}
