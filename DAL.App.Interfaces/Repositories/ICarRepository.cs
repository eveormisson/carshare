﻿using DAL.Interfaces.Repositories;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface ICarRepository : IRepository<Car>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        List<Car> FindByBrand(string search);

        List<Car> FindByOwner(string ownerId);

        Task<Car> GetBelongingToUserAsync(int carId, string userId);

        Task<List<Car>> AllBelongingToUserAsync(string userId);

        Task<Car> FindWithUsersStatusesAsync(int id);

        Task<Car> GetBelongingToUserWithStatusUserAsync(int carId, string userId);

        List<Car> FindAllCars();
    }

}
