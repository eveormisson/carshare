﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IConditionOfUseRepository : IRepository <ConditionOfUse>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        Task<ConditionOfUse> GetBelongingToUserAsync(int conditionOfUseId, string userId);

        Task<List<ConditionOfUse>> AllBelongingToUserAsync(string userId);

        Task<ConditionOfUse> FindWithUsersCarsAsync(int id);

        Task<ConditionOfUse> GetBelongingToUserWithCarAsync(int conditionOfUseId, string userId);
    }
}
