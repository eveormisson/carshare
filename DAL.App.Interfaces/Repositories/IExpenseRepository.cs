﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IExpenseRepository : IRepository<Expense>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        Task<Expense> GetBelongingToUserAsync(int expenseId, string userId);

        Task<List<Expense>> AllBelongingToUserAsync(string userId);

        Task<Expense> FindWithUsersCarsStatusesAsync(int id);

        Task<Expense> GetBelongingToUserWithStatusCarAsync(int expenseId, string userId);
    }
}
