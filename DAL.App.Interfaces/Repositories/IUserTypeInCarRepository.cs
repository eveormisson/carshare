﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IUserTypeInCarRepository : IRepository<UserTypeInCar>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        Task<List<UserTypeInCar>> AllBelongingToUserAsync(string userId);

        void AddUserToCar(string userId, int carId);
    }
}
