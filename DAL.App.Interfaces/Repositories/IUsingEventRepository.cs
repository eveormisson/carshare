﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IUsingEventRepository : IRepository<UsingEvent>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        List<UsingEvent> FindByDrivingDestination(string search);

        Task<List<UsingEvent>> AllBelongingToUserAsync(string userId);

        Task<UsingEvent> GetBelongingToUserAsync(int usingEventId, string userId);

        Task<UsingEvent> FindWithUsersCarsStatusesAsync(int id);

        Task<UsingEvent> GetBelongingToUserWithCarsUsersStatusesAsync(int usingEventId, string userId);
    }
}
