﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IEventStatusRepository : IRepository<EventStatus>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        Task<EventStatus> FindTranslationAsync(int id);
    }
}
