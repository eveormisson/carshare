﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IApplicationUserRepository : IRepository<ApplicationUser>
    {
        bool ExistsByPrimaryKey(string keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(string keyValue);

        Task<ApplicationUser> FindByGuidAsync(string id);

        List<ApplicationUser> GetAllApplicationUsers();
    }
}
