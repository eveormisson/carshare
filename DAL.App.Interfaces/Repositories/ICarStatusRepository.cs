﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface ICarStatusRepository : IRepository <CarStatus>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        Task<CarStatus> FindTranslationAsync(int id);
    }
}
