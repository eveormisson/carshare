﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IUserTypeRepository : IRepository<UserType>
    {
        bool ExistsByPrimaryKey(int keyValue);

        Task<bool> ExistsByPrimaryKeyAsync(int keyValue);

        List<UserTypeInCar> FindUserType(string search);

        Task<UserType> FindTranslationAsync(int id);
    }
}
